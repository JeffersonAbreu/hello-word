from flet import *

def main(page: Page):
    page.theme_mode = ThemeMode.DARK
    page.title = "Rotas"
    page.navigation_bar = NavigationBar(
        destinations=[
            NavigationDestination(icon=icons.HOME, label="Home"),
            NavigationDestination(icon=icons.EXPLORE, label="Explorar"),
            NavigationDestination(icon=icons.COMMUTE, label="Rotas"),
        ]
    )
    page.add(Text("Corpo da Aplicação"))

app(target=main)