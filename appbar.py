from flet import *

def main(page: Page):
    page.theme_mode = ThemeMode.DARK
    page.appbar = AppBar(
        leading=Icon(icons.CODE),
        #leading_width=40,
        title=Text("AppBar Exemplo"),
        bgcolor="#003377",
        #center_title=True,
        actions=[
            IconButton(icons.WB_SUNNY_OUTLINED),
            PopupMenuButton(
                items=[
                    PopupMenuItem(text="Login"),
                    PopupMenuItem(),
                    PopupMenuItem(text="Sair"),
                ]
            )
        ]

    )
    page.add(Text("Corpo"))

app(target=main)